package ru.itis.demoaspects.aspects.impl;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Slf4j
@Component
public class LoggableAspect {


    @Pointcut("(execution(* *(..)) || execution(*.new(..))) " +
            "&& @annotation(ru.itis.demoaspects.aspects.annotations.Loggable)")
//    @Pointcut("@annotation(ru.itis.demoaspects.aspects.annotations.Loggable)")
    public void methodPointcut() {}

    @Around("methodPointcut()")
    public Object logMethod(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        log.info("Called method {} with {}", proceedingJoinPoint.getSignature(), proceedingJoinPoint.getArgs());
        long start = System.currentTimeMillis();
        Object res = proceedingJoinPoint.proceed();
        long finish = System.currentTimeMillis();
        log.info("Method executed within {} ms, returned {}", finish - start, res);
        return res;
    }

    @Before("call(* *.method2(..))")
    public void log(JoinPoint joinPoint) {
        log.info("calling method2");
    }
}




