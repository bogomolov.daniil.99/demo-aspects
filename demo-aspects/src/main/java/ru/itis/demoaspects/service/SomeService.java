package ru.itis.demoaspects.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.itis.demoaspects.aspects.annotations.Loggable;

import javax.annotation.Resource;

@Service
@Slf4j
public class SomeService {

    private int random = 0;


    @Loggable
    public int method1() {
//       return service.method2();
        return method2();
    }


    @Loggable
    private int method2() {
        log.info("called method2");
        return ++random;
    }

    // private final LoggableAspect aspect;

    //    @Loggable
    //    public int method1() {
    //         make JP
    //        return aspect.logMethod();
    //    }

    //    private ajc_closure() {
//          return method2();
//          }
//
//
}




