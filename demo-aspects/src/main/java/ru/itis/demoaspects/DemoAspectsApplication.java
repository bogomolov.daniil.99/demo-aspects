package ru.itis.demoaspects;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import ru.itis.demoaspects.models.Human;
import ru.itis.demoaspects.service.SomeService;

@SpringBootApplication
public class DemoAspectsApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(DemoAspectsApplication.class, args);
        context.getBean(SomeService.class).method1();
        Human human = new Human("123");
    }

}
