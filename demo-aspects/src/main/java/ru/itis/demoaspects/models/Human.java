package ru.itis.demoaspects.models;

import ru.itis.demoaspects.aspects.annotations.Loggable;

public class Human {
    private String name;

    @Loggable
    public Human(String name) {
        this.name = name;
    }
}
